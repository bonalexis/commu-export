enum FontSize {
  XS = "text-xs",
  SM = "text-sm",
  BASE = "text-base",
  LG = "text-lg",
  XL = "text-xl",
  "2XL" = "text-2xl",
  "3XL" = "text-3xl",
  "4XL" = "text-4xl",
  "5XL" = "text-5xl",
  "6XL" = "text-6xl",
  "7XL" = "text-7xl",
  "8XL" = "text-8xl",
  "9XL" = "text-9xl",
}

export default FontSize;
