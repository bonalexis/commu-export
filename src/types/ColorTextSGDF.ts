enum ColorTextSGDF {
  INSTITUTION = "text-sgdf-institution",
  FARFA = "text-sgdf-farfa",
  ORANGE = "text-sgdf-",
  BLEU = "text-sgdf-bleu",
  ROUGE = "text-sgdf-rouge",
  COMPA = "text-sgdf-compa",
  VIOLET = "text-sgdf-violet",
  BLANC = "text-white",
}

export default ColorTextSGDF;
