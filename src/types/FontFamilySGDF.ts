enum FontFamilySGDF {
  SARABUN = "font-sarabun",
  CAVEAT_BRUSH = "font-caveat-brush",
  RALEWAY = "font-raleway",
}

export default FontFamilySGDF;
