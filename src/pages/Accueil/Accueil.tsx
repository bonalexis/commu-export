import H1 from "../../components/Typographie/Headings/H1";
import H2 from "../../components/Typographie/Headings/H2";
import Paragraph from "../../components/Typographie/Paragraph/Paragraph";
import CommonPage from "../../layouts/CommonPage/CommonPage";
import imgAccueil from "../../assets/images/accueil.jpg";
import H3 from "../../components/Typographie/Headings/H3";
import { Link } from "react-router-dom";
import { routes } from "../../router";
import classNames from "../../utils/classNames";

export default function Accueil() {
  return (
    <CommonPage>
      <div className="mx-4 flex flex-col gap-4 my-8">
        <H1 className="self-center">Commu Export</H1>
        <H2 className="self-center">Récupérer les coordonnées des individus</H2>
        <Paragraph>
          Cet outil vous permet de récupérer des fiches de coordonnées depuis un
          export d'individus réalisé sur l'intranet des Scouts et Guides de
          France. (voir{" "}
          <Link to={routes.AIDE_INTRANET} className="underline">
            Aide Intranet
          </Link>
          )
        </Paragraph>
        <section className="my-4 flex flex-col gap-4">
          <H3>Prérequis</H3>
          <Paragraph>
            Pour visualiser et récupérer les fiches de coordonnées des membres
            et de leurs parents, vous devez obtenir un export de ce fichier via
            l'intranet des Scouts et Guides de France.
          </Paragraph>
        </section>
        <section className="my-4 flex flex-col gap-4">
          <H3>Importer des individus</H3>
          <Paragraph>
            Avant de continuer, sachez qu'aucune donnée importée ici ne sera
            transmise à un tiers. C'est votre propre ordinateur qui effectuera
            les calculs nécessaires à la conversion du tableur en fiches
            coordonnées.
          </Paragraph>
          <Paragraph>
            Pour poursuivre, utilisez le bouton ci-dessous pour sélectionner le
            fichier contenant des individus au format .xls via votre explorateur
            de fichiers.
          </Paragraph>
          <input
            type="file"
            accept=".xls"
            name="file-input"
            id="file-input"
            className={classNames(
              "self-center border text-sgdf-institution text-lg border-sgdf-institution shadow-sm focus:z-10 focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none cursor-pointer",
              "file:bg-sgdf-institution file:text-white file:border-0 file:me-4 file:py-2 file:px-4 file:"
            )}
          />
        </section>

        <section className="my-4 flex flex-col gap-4">
          <H3>Fiches coordonnées</H3>
          <Paragraph>
            Si aucune erreur n'a été détectée lors de la lecture de votre
            fichier, vous pouvez visualiser chaque fiche ici et choisir celles à
            télécharger individuellement ou récupérer toutes les coordonnées.
            Vous pouvez nommer cet import et cela créera une étiquette pour tous
            les contacts.
          </Paragraph>
        </section>
      </div>
      <img src={imgAccueil} alt="Image" />
    </CommonPage>
  );
}
