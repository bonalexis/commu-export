/**
 * Créer un slug à partir d'un titre donné. Un slug est une chaîne de texte simplifiée et normalisée, généralement utilisée pour créer des URL lisibles et SEO-friendly.
 *
 * @param {string} title - Le titrer original à transformer en slug.
 * @returns {string} - Un slug simplifié et normalisé du titre d'entrée.
 */
export default function createSlug(title: string): string {
  return title
    .toLowerCase()
    .replace(/[^a-z0-9 ]/g, "")
    .replace(/\s+/g, "-");
}
