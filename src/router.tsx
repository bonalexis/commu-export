import { createBrowserRouter } from "react-router-dom";
import Accueil from "./pages/Accueil/Accueil";
import AideIntranet from "./pages/AideIntranet/AideIntranet";

export enum routes {
  ACCUEIL = "/",
  AIDE_INTRANET = "/aide-intranet",
}

const router = createBrowserRouter([
  { path: routes.ACCUEIL, element: <Accueil /> },
  { path: routes.AIDE_INTRANET, element: <AideIntranet /> },
]);

export default router;
