import { Link } from "react-router-dom";
import imgLogoWhite from "../../assets/images/logo/logo-commu-export-white.svg";

export default function Header() {
  return (
    <header className="bg-sgdf-institution sticky top-0 flex justify-center">
      <div className="max-w-screen-md w-full px-4 py-2">
        <Link to="/" className="flex gap-2 items-center">
          <img src={imgLogoWhite} className="size-10" />
          <span className="text-white font-caveat-brush text-3xl">
            Commu Export
          </span>
        </Link>
      </div>
    </header>
  );
}
