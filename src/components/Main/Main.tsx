import { PropsWithChildren } from "react";

export default function Main({ children }: PropsWithChildren) {
  return (
    <main className="flex justify-center">
      <div className="max-w-screen-md flex flex-col w-full">{children}</div>
    </main>
  );
}
