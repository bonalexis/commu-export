import ColorTextSGDF from "../../../types/ColorTextSGDF";
import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import Text, { TextProps } from "../Text/Text";

export default function Paragraph({
  color = ColorTextSGDF.INSTITUTION,
  font = FontFamilySGDF.SARABUN,
  size = FontSize.BASE,
  className,
  children,
}: TextProps) {
  const textProps = { color, font, size, className };

  return (
    <Text {...textProps} as="p">
      {children}
    </Text>
  );
}
