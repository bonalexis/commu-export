import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H4({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["3XL"],
  className,
  children,
}: TextProps) {
  const h4Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h4 {...h4Props}>
      <Text {...textProps}>{children}</Text>
    </h4>
  );
}
