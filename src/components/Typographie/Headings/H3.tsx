import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H3({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["4XL"],
  className,
  children,
}: TextProps) {
  const h3Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h3 {...h3Props}>
      <Text {...textProps}>{children}</Text>
    </h3>
  );
}
