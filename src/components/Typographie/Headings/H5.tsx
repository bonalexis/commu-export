import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H5({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["2XL"],
  className,
  children,
}: TextProps) {
  const h5Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h5 {...h5Props}>
      <Text {...textProps}>{children}</Text>
    </h5>
  );
}
