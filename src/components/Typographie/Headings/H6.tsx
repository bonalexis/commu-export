import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H6({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["XL"],
  className,
  children,
}: TextProps) {
  const h6Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h6 {...h6Props}>
      <Text {...textProps}>{children}</Text>
    </h6>
  );
}
