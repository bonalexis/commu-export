import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H2({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["5XL"],
  className,
  children,
}: TextProps) {
  const h2Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h2 {...h2Props}>
      <Text {...textProps}>{children}</Text>
    </h2>
  );
}
