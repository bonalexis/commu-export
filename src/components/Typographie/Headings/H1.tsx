import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";
import createSlug from "../../../utils/createSlug";
import Text, { TextProps } from "../Text/Text";

export default function H1({
  color,
  font = FontFamilySGDF.CAVEAT_BRUSH,
  size = FontSize["6XL"],
  className,
  children,
}: TextProps) {
  const h1Props = {
    id: createSlug(children?.toLocaleString() || ""),
    className,
  };
  const textProps = { color, font, size };

  return (
    <h1 {...h1Props}>
      <Text {...textProps}>{children}</Text>
    </h1>
  );
}
