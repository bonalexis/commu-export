import { ComponentPropsWithRef, ElementType, PropsWithChildren } from "react";
import ColorTextSGDF from "../../../types/ColorTextSGDF";
import classNames from "../../../utils/classNames";
import FontFamilySGDF from "../../../types/FontFamilySGDF";
import FontSize from "../../../types/FontSize";

export type TextProps = PropsWithChildren<{
  color?: ColorTextSGDF;
  font?: FontFamilySGDF;
  size?: FontSize;
  as?: ComponentPropsWithRef<ElementType>;
  className?: string;
}>;

export default function Text({
  color = ColorTextSGDF.INSTITUTION,
  font = FontFamilySGDF.SARABUN,
  size = FontSize.BASE,
  as: AsElement,
  className,
  children,
}: TextProps) {
  const textClassNames = classNames(color, font, size, className);

  if (AsElement !== undefined) {
    return <AsElement className={textClassNames}>{children}</AsElement>;
  }

  return <span className={textClassNames}>{children}</span>;
}
