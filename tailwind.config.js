/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Sarabun", "sans-serif"],
        "caveat-brush": ["Caveat Brush", "cursive"],
        raleway: ["Raleway", "sans-serif"],
      },
      colors: {
        "sgdf-institution": "#003a5d",
        "sgdf-farfa": "#65bc99",
        "sgdf-orange": "#ff8300",
        "sgdf-bleu": "#0077b3",
        "sgdf-rouge": "#d03f15",
        "sgdf-compa": "#007254",
        "sgdf-violet": "#6e74aa",
      },
    },
  },
  plugins: [],
};
